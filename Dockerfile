FROM debian:stretch-slim
MAINTAINER asmaloney@gmail.com

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get -y update && \
  apt-get -y install git libstdc++6 && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/man/?? /usr/share/man/??_*

CMD ["git", "help"]
